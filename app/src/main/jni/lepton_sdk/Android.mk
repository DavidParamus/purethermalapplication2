LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_SHARED_LIBRARIES += libusb1.0

LOCAL_C_INCLUDES := \
	$(LOCAL_PATH)/ \
	$(LOCAL_PATH)/Inc

LOCAL_SRC_FILES := \
     Src/crc16fast.c \
     Src/LEPTON_AGC.c \
   	 Src/LEPTON_OEM.c \
   	 Src/LEPTON_RAD.c \
   	 Src/LEPTON_SDK.c \
   	 Src/LEPTON_SYS.c \
   	 Src/LEPTON_VID.c

LOCAL_MODULE := leptonsdk

common_CFLAGS := -Wall -fexceptions

include $(BUILD_SHARED_LIBRARY)