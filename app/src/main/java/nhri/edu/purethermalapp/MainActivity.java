package nhri.edu.purethermalapp;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.PendingIntent;
import android.content.Intent;
import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbManager;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;

import nhri.edu.purethermalapp.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    private static final String ACTION_USB_PERMISSION = "nhri.edu.purethermalapp.USB_PERMISSION";

    private static int ActivityStartIsoStreamRequestCode = 2;

    // Video interface subclass codes:
    private static final int SC_VIDEOCONTROL = 0x01;
    private static final int SC_VIDEOSTREAMING = 0x02;
    private static final String DEFAULT_USBFS = "/dev/bus/usb";


    // Used to load the 'mydevice' library on application startup.
    private static boolean isLoaded;
    static {

        System.loadLibrary("usb1.0");
        System.loadLibrary("yuv");
        System.loadLibrary("jpeg");
        System.loadLibrary("jpeg-turbo");
        System.loadLibrary("Usb_Support");
        isLoaded = true;
    }

    private ActivityMainBinding binding;
    private PendingIntent mPermissionIntent;

    /* USB Device */
    private UsbManager usbManager;
    private UsbDevice flirDevice;
    private int vInterfaceNumber = 2;
    private UsbDeviceConnection vConnection;
    private UsbInterface vInterface;
    private UsbEndpoint vPointRead = null;
    private UsbEndpoint vPointWrite = null;
    private static String mUsbFs;


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        usbManager = (UsbManager) getSystemService(this.getApplicationContext().USB_SERVICE);
        HashMap<String, UsbDevice> deviceList = usbManager.getDeviceList();
        Iterator<UsbDevice> deviceIterator = deviceList.values().iterator();
        while (deviceIterator.hasNext()) {
            UsbDevice device = deviceIterator.next();
            Log.d("[DEBUG]" + "List USB Devices", "Device Info:" + showDeviceInfo(device));
            if (device.getVendorId() == 7758 && device.getProductId() == 256) {
                flirDevice = device;
            }
        }
        if (flirDevice != null) {
            if (!usbManager.hasPermission(flirDevice)) {
                mPermissionIntent = PendingIntent.getBroadcast(this.getApplicationContext(), 0, new Intent(ACTION_USB_PERMISSION), 0);
                usbManager.requestPermission(flirDevice, mPermissionIntent);
            }
            for (int i = 0; i < flirDevice.getInterfaceCount(); i++) {
                Log.d("[DEBUG]" + "List USB Devices", "interface Info:" + flirDevice.getInterface(i));
            }
        }

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        // Example of a call to a native method
        TextView tv = binding.sampleText;

        Button button = binding.button;
        button.setOnClickListener(view -> {
            Intent intent = new Intent(this, StartIsoStreamActivity.class);
            startActivityForResult(intent, ActivityStartIsoStreamRequestCode);
        });

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private String showDeviceInfo(UsbDevice device) {
        return new StringBuilder()
                .append("{\n")
                .append("    \"Device ID\": \"").append(device.getDeviceId()).append("\"\n")
                .append("    \"Device Name\": \"").append(device.getDeviceName()).append("\"\n")
                .append("    \"Device Product Id\": \"").append(device.getProductId()).append("\"\n")
                .append("    \"Device Product Name\": \"").append(device.getProductName()).append("\"\n")
                .append("    \"Device Vendor ID\": \"").append(device.getVendorId()).append("\"\n")
                .append("    \"Manufacturer Name\": \"").append(device.getManufacturerName()).append("\"\n")
                .append("    \"Device Class\": \"").append(device.getDeviceClass()).append("\"\n")
                .append("    \"Device SubClass\": \"").append(device.getDeviceSubclass()).append("\"\n")
                .append("    \"Device Protocol\": \"").append(device.getDeviceProtocol()).append("\"\n")
                .append("    \"Device Version\": \"").append(device.getVersion()).append("\"\n")
                .append("    \"Serial Number\": \"").append(device.getSerialNumber()).append("\"\n")
                .append("    \"Device Protocol\": \"").append(device.getDeviceProtocol()).append("\"\n")
                .append("}\n")
                .toString();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(vConnection != null){
            vConnection.releaseInterface(vInterface);
            vConnection.close();
            vConnection = null;
        }
    }

    private final String getUSBFSName(final UsbDevice ctrlBlock) {
        String result = null;
        final String name = ctrlBlock.getDeviceName();
        final String[] v = !TextUtils.isEmpty(name) ? name.split("/") : null;
        if ((v != null) && (v.length > 2)) {
            final StringBuilder sb = new StringBuilder(v[0]);
            for (int i = 1; i < v.length - 2; i++)
                sb.append("/").append(v[i]);
            result = sb.toString();
        }
        if (TextUtils.isEmpty(result)) {
            Log.e("[ERROR]", "failed to get USBFS path, try to use default path:" + name);
            result = DEFAULT_USBFS;
        }
        return result;
    }

}