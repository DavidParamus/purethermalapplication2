/*
Copyright 2019 Peter Stoiber

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

Please contact the author if you need another license.
This Repository is provided "as is", without warranties of any kind.
*/

package nhri.edu.purethermalapp;

import static java.lang.Integer.parseInt;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.sun.jna.Pointer;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Formatter;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import nhri.edu.purethermalapp.LibUsb.JNA_I_LibUsb;
import nhri.edu.purethermalapp.LibUsb.StartIsoStreamService;
import nhri.edu.purethermalapp.UVC_Descriptor.IUVC_Descriptor;
import nhri.edu.purethermalapp.UVC_Descriptor.UVC_Descriptor;
import nhri.edu.purethermalapp.UVC_Descriptor.UVC_Initializer;
import nhri.edu.purethermalapp.UsbIso64.USBIso;
import nhri.edu.purethermalapp.UsbIso64.usbdevice_fs_util;

public class StartIsoStreamActivity extends Activity {

    private static final String ACTION_USB_PERMISSION = "nhri.edu.purethermalapp.USB_PERMISSION";
    // USB codes:
    // Request types (bmRequestType):
    private static final int RT_STANDARD_INTERFACE_SET = 0x01;
    private static final int RT_CLASS_INTERFACE_SET = 0x21;
    private static final int RT_CLASS_INTERFACE_GET = 0xA1;
    // Video interface subclass codes:
    private static final int SC_VIDEOCONTROL = 0x01;
    private static final int SC_VIDEOSTREAMING = 0x02;
    // Standard request codes:
    private static final int SET_INTERFACE = 0x0b;
    // Video class-specific request codes:
    private static final int SET_CUR = 0x01;
    private static final int GET_CUR = 0x81;
    private static final int GET_MIN = 0x82;
    private static final int GET_MAX = 0x83;
    private static final int GET_RES = 0x84;
    // VideoControl interface control selectors (CS):
    private static final int VC_REQUEST_ERROR_CODE_CONTROL = 0x02;
    // VideoStreaming interface control selectors (CS):
    private static final int VS_PROBE_CONTROL = 0x01;
    private static final int VS_COMMIT_CONTROL = 0x02;
    private static final int VS_STILL_PROBE_CONTROL = 0x03;
    private static final int VS_STILL_COMMIT_CONTROL = 0x04;
    private static final int VS_STREAM_ERROR_CODE_CONTROL = 0x06;
    private static final int VS_STILL_IMAGE_TRIGGER_CONTROL = 0x05;

    // LEPTON Control ID
    private static final int LEP_CID_AGC_MODULE = 0x0100;
    private static final int LEP_CID_OEM_MODULE = 0x0800;
    private static final int LEP_CID_RAD_MODULE = 0x0E00;
    private static final int LEP_CID_SYS_MODULE = 0x0200;
    private static final int LEP_CID_VID_MODULE = 0x0300;

    private static final int VC_CONTROL_XU_LEP_AGC_ID = 3;
    private static final int VC_CONTROL_XU_LEP_OEM_ID = 4;
    private static final int VC_CONTROL_XU_LEP_RAD_ID = 5;
    private static final int VC_CONTROL_XU_LEP_SYS_ID = 6;
    private static final int VC_CONTROL_XU_LEP_VID_ID = 7;

    // Lepton Variables
    private static final int LEP_RAD_MODULE_BASE = 0x4E00;   // includes the OEM Bit set 0x4000
    private static final int LEP_CID_RAD_SPOTMETER_OBJ_KELVIN = LEP_RAD_MODULE_BASE + 0x00D0;

    private class LEP_RAD_SPOTMETER_OBJ_KELVIN_T {
        int radSpotmeterValue;
        int radSpotmeterMaxValue;
        int radSpotmeterMinValue;
        int radSpotmeterPopulation;
    }

    // Android USB Classes
    private UsbManager usbManager;
    private UsbDevice camDevice = null;
    private UsbDeviceConnection camDeviceConnection;
    private UsbInterface camControlInterface;
    private UsbInterface camStreamingInterface;
    private UsbEndpoint camStreamingEndpoint;
    private boolean bulkMode;
    private PendingIntent mPermissionIntent;

    // Camera Values
    public static int camStreamingAltSetting;
    public static int camFormatIndex;
    public int camFrameIndex;
    public static int camFrameInterval;
    public static int packetsPerRequest;
    public static int maxPacketSize;
    public int imageWidth;
    public int imageHeight;
    public static int activeUrbs;
    public static String videoformat;
    public static boolean camIsOpen;
    public static byte bUnitID;
    public static byte bTerminalID;
    public static byte bStillCaptureMethod;
    public static byte[] bNumControlTerminal;
    public static byte[] bNumControlUnit;
    public static byte[] bcdUVC;
    public static byte[] bcdUSB;
    public static boolean LIBUSB;
    public static boolean moveToNative;

    // Vales for debuging the camera
    private boolean imageCapture = false;
    private boolean videorecord = false;
    private boolean videorecordApiJellyBeanNup = false;
    private boolean stopKamera = false;
    private boolean pauseCamera = false;
    private boolean longclickVideoRecord = false;
    private boolean stillImageAufnahme = false;
    private boolean saveStillImage = false;
    private String controlltransfer;
    private boolean exit = false;
    public StringBuilder stringBuilder;
    private int[] convertedMaxPacketSize;
    private boolean lowerResolution;

    public static enum Videoformat {YUV, MJPEG, YUY2, YV12, YUV_422_888, YUV_420_888, NV21, UYVY}

    // Buttons & Views
    private TextView textView;
    protected ImageView imageView;
    private TextView textView2;
    protected ImageView imageView2;
    protected Button startStream;
    protected Button menu;
    protected Button stopStreamButton;
    protected ImageButton photoButton;
    protected ToggleButton videoButton;
    private TextView tv;
    private Date date;
    private SimpleDateFormat dateFormat;
    private File file;

    // Time Values
    int lastPicture = 0; // Current picture counter
    int lastVideo = 0; // Current video file counter
    long startTime;
    long currentTime;
    double maxTemperature;
    double minTemperature;

    // Other Classes
    private MJPEGGenerator generator;
    private BitmapToVideoEncoder bitmapToVideoEncoder;
    private volatile IsochronousStream runningStream;
    private volatile IsochronousStreamLibUsb runningStreamLibUsb;
    private SeekBar simpleSeekBar;
    private Button defaultButton;
    private Switch switchAuto;

    // Camera Configuration Values to adjust Values over Controltransfers
    private boolean focusAutoState;
    private boolean exposureAutoState;
    float discrete = 0;
    static float start;
    static float end;
    float start_pos;
    int start_position = 0;
    private int framecount = 0;

    // UVC Interface
    private static IUVC_Descriptor iuvc_descriptor;

    private int[] differentFrameSizes;
    private int[] lastThreeFrames;
    private int whichFrame = 0;
    private String msg;
    private int rotate = 0;
    private boolean horizontalFlip;
    private boolean verticalFlip;

    // NEW LIBUSB VALUES
    private static int fd;
    private static int productID;
    private static int vendorID;
    private static String adress;
    private static int camStreamingEndpointAdress;
    private static String mUsbFs;
    private static int busnum;
    private static int devaddr;
    private volatile boolean libusb_is_initialized;
    private static final String DEFAULT_USBFS = "/dev/bus/usb";

    // JNI METHODS
    public native void JniIsoStreamActivitySurface(final Surface surface, int a, int b);

    public native void JniIsoStreamActivity(int a, int b);

    public native void JniSetSurfaceView(final Surface surface);

    public native void JniSetSurfaceYuv(final Surface surface);

    // Bitmap Method
    public native void frameToBitmap(Bitmap bitmap);

    public native void YUY2pixeltobmp(byte[] uyvy_data, Bitmap bitmap, int im_width, int im_height);

    public native void UYVYpixeltobmp(byte[] uyvy_data, Bitmap bitmap, int im_width, int im_height);

    //public native void JniProbeCommitControl(int bmHint,int camFormatIndex,int camFrameIndex,int  camFrameInterval);
    private Surface mPreviewSurface;
    private SurfaceView mUVCCameraView;

    private static StartIsoStreamActivity instance;

    private static boolean isLoaded;

    static {
        if (!isLoaded) {
            System.loadLibrary("usb1.0");
            System.loadLibrary("yuv");
            System.loadLibrary("jpeg");
            System.loadLibrary("jpeg-turbo");
            System.loadLibrary("leptonsdk");
            System.loadLibrary("Usb_Support");
            isLoaded = true;
        }
    }

    private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (ACTION_USB_PERMISSION.equals(action)) {
                synchronized (this) {
                    camDevice = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                    if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                        if (camDevice != null) {
                            log("camDevice from BraudcastReceiver");
                        }
                    } else {
                        log("permission denied for device " + camDevice);
                        displayMessage("permission denied for device " + camDevice);
                    }
                }
            }
        }
    };

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                log("BroadcastReceiver receiver");
                int resultCode = bundle.getInt(StartIsoStreamService.RESULT);
                if (resultCode == RESULT_OK) {

                } else {
                    displayMessage("Stream failed");
                }
            }
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        Log.v("STATE", "onStart() is called");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopTheCameraStreamClickEvent();
        try {
            unregisterReceiver(mUsbReceiver);
            unregisterReceiver(receiver);
        } catch (Exception e) {
            log("Exception = " + e);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        beenden(false);
    }


    public static StartIsoStreamActivity getInstance() {
        if (instance == null) {
            instance = new StartIsoStreamActivity();
        }
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /* UI initialize */
        setContentView(R.layout.iso_stream_layout);
        textView = findViewById(R.id.textView);
        imageView = findViewById(R.id.imageView);
        textView2 = findViewById(R.id.textView2);
        imageView2 = findViewById(R.id.imageView2);

        stopStreamButton = findViewById(R.id.stop_stream_btn);
        stopStreamButton.setOnClickListener(view -> finish());
        /* USB initialize */
        usbManager = (UsbManager) getSystemService(getApplicationContext().USB_SERVICE);
        fetchTheValues();
        mPermissionIntent = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_USB_PERMISSION), 0);

        if (camDevice == null) {
            try {
                findCam();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        try {
            openCam(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        initStillImageParms();
        if (camIsOpen) {
            if (runningStream != null) {
                return;
            }
            runningStream = new StartIsoStreamActivity.IsochronousStream(this);
            runningStream.start();
            byte[] a = camDeviceConnection.getRawDescriptors();
            ByteBuffer uvcData = ByteBuffer.wrap(a);
            UVC_Descriptor uvc_descriptor = new UVC_Descriptor(uvcData);
            if (uvc_descriptor.phraseUvcData() == 0) {
                iuvc_descriptor = new UVC_Initializer(uvc_descriptor);
                log("videoformat = " + videoformat);
                if (videoformat.equals("MJPEG")) {
                    log("Arrays.deepToString(iuvc_descriptor.findDifferentResolutions(false)) = " + Arrays.deepToString(iuvc_descriptor.findDifferentResolutions(true)));
                    log("");
                    log("iuvc_descriptor.findDifferentFrameIntervals( = " + Arrays.toString(iuvc_descriptor.findDifferentFrameIntervals(true, new int[]{imageWidth, imageHeight})));
                    log("");
                } else {
                    log("Arrays.deepToString(iuvc_descriptor.findDifferentResolutions(false)) = " + Arrays.deepToString(iuvc_descriptor.findDifferentResolutions(false)));
                    log("");
                    log("iuvc_descriptor.findDifferentFrameIntervals( = " + Arrays.toString(iuvc_descriptor.findDifferentFrameIntervals(false, new int[]{imageWidth, imageHeight})));
                    log("");
                }
            } else displayMessage("Interface initialization for the Descriptor failed.");
        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (msg != null) displayMessage(msg);
                    else displayMessage("Failed to start the Camera Stream");
                    msg = null;
                }
            });
        }
    }

    public void beenden(boolean exit) {
        if (LIBUSB) {
            if (libusb_is_initialized) {
                JNA_I_LibUsb.INSTANCE.stopStreaming();
            }
            //I_LibUsb.INSTANCE.closeLibUsb();
            //I_LibUsb.INSTANCE.exit();
            camDevice = null;
            Intent resultIntent = new Intent();
            if (exit == true) resultIntent.putExtra("closeProgram", true);
            setResult(Activity.RESULT_OK, resultIntent);
            //mService.streamCanBeResumed = false;
            finish();
        } else {
            if (camIsOpen) {
                closeCameraDevice();
            } else if (camDeviceConnection != null) {
                camDeviceConnection.releaseInterface(camControlInterface);
                camDeviceConnection.releaseInterface(camStreamingInterface);
                camDeviceConnection.close();
            }
            Intent resultIntent = new Intent();
            if (exit == true) resultIntent.putExtra("closeProgram", true);
            setResult(Activity.RESULT_OK, resultIntent);
            //mService.streamCanBeResumed = false;
            finish();
        }
    }

    // Start the Stream
    public void isoStream() {

        try {
            openCam(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        initStillImageParms();
        if (camIsOpen) {
            if (runningStream != null) {
                return;
            }
            runningStream = new IsochronousStream(this);
            runningStream.start();
            byte[] a = camDeviceConnection.getRawDescriptors();
            ByteBuffer uvcData = ByteBuffer.wrap(a);
            UVC_Descriptor uvc_descriptor = new UVC_Descriptor(uvcData);
            if (uvc_descriptor.phraseUvcData() == 0) {
                iuvc_descriptor = new UVC_Initializer(uvc_descriptor);
                log("videoformat = " + videoformat);
                if (videoformat.equals("MJPEG")) {
                    log("Arrays.deepToString(iuvc_descriptor.findDifferentResolutions(false)) = " + Arrays.deepToString(iuvc_descriptor.findDifferentResolutions(true)));
                    log("");
                    log("iuvc_descriptor.findDifferentFrameIntervals( = " + Arrays.toString(iuvc_descriptor.findDifferentFrameIntervals(true, new int[]{imageWidth, imageHeight})));
                    log("");
                } else {
                    log("Arrays.deepToString(iuvc_descriptor.findDifferentResolutions(false)) = " + Arrays.deepToString(iuvc_descriptor.findDifferentResolutions(false)));
                    log("");
                    log("iuvc_descriptor.findDifferentFrameIntervals( = " + Arrays.toString(iuvc_descriptor.findDifferentFrameIntervals(false, new int[]{imageWidth, imageHeight})));
                    log("");
                }
            } else displayMessage("Interface initialization for the Descriptor failed.");

        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (msg != null) displayMessage(msg);
                    else displayMessage("Failed to start the Camera Stream");
                    msg = null;
                }
            });
        }
    }

    private void findCam() throws Exception {
        camDevice = findCameraDevice();
        if (camDevice == null) {
            camDevice = checkDeviceVideoClass();
            if (camDevice == null) throw new Exception("No USB camera device found.");
        }
        if (!usbManager.hasPermission(camDevice)) {
            log("Asking for Permissions");
            usbManager.requestPermission(camDevice, mPermissionIntent);
        } else usbManager.requestPermission(camDevice, mPermissionIntent);
    }

    private UsbDevice checkDeviceVideoClass() {
        HashMap<String, UsbDevice> deviceList = usbManager.getDeviceList();
        log("USB devices count = " + deviceList.size());
        for (UsbDevice usbDevice : deviceList.values()) {
            log("USB device \"" + usbDevice.getDeviceName() + "\": " + usbDevice);
            if (usbDevice.getDeviceClass() == 14 && usbDevice.getDeviceSubclass() == 2) {
                moveToNative = true;
                return usbDevice;
            } else if (usbDevice.getDeviceClass() == 239 && usbDevice.getDeviceSubclass() == 2) {
                moveToNative = true;
                return usbDevice;
            }
            if (checkDeviceHasVideoControlInterface(usbDevice)) {
                return usbDevice;
            }
        }
        return null;
    }

    private UsbDevice findCameraDevice() {
        HashMap<String, UsbDevice> deviceList = usbManager.getDeviceList();
        log("USB devices count = " + deviceList.size());
        for (UsbDevice usbDevice : deviceList.values()) {
            if (moveToNative) {
                if (checkDeviceHasVideoControlInterface(usbDevice)) {
                    return usbDevice;
                }
            } else {
                log("USB device \"" + usbDevice.getDeviceName() + "\": " + usbDevice);
                if (checkDeviceHasVideoStreamingInterface(usbDevice)) {
                    return usbDevice;
                }
            }
        }
        return null;
    }

    private boolean checkDeviceHasVideoStreamingInterface(UsbDevice usbDevice) {
        return getVideoStreamingInterface(usbDevice) != null;
    }

    private boolean checkDeviceHasVideoControlInterface(UsbDevice usbDevice) {
        return getVideoControlInterface(usbDevice) != null;
    }

    private UsbInterface getVideoControlInterface(UsbDevice usbDevice) {
        return findInterface(usbDevice, UsbConstants.USB_CLASS_VIDEO, SC_VIDEOCONTROL, false);
    }

    private UsbInterface getVideoStreamingInterface(UsbDevice usbDevice) {
        return findInterface(usbDevice, UsbConstants.USB_CLASS_VIDEO, SC_VIDEOSTREAMING, true);
    }

    private UsbInterface findInterface(UsbDevice usbDevice, int interfaceClass, int interfaceSubclass, boolean withEndpoint) {
        int interfaces = usbDevice.getInterfaceCount();
        for (int i = 0; i < interfaces; i++) {
            UsbInterface usbInterface = usbDevice.getInterface(i);
            if (usbInterface.getInterfaceClass() == interfaceClass && usbInterface.getInterfaceSubclass() == interfaceSubclass && (!withEndpoint || usbInterface.getEndpointCount() > 0)) {
                return usbInterface;
            }
        }
        return null;
    }

    private void openCam(boolean init) throws Exception {
        openCameraDevice(init);
        if (init) {
            initCamera();
            camIsOpen = true;
        }
        log("Camera opened sucessfully");
    }

    private void openCameraDevice(boolean init) throws Exception {
        if (moveToNative) {
            camDeviceConnection = usbManager.openDevice(camDevice);
            int FD = camDeviceConnection.getFileDescriptor();
            if (camStreamingEndpointAdress == 0) {
                camStreamingEndpointAdress = JNA_I_LibUsb.INSTANCE.fetchTheCamStreamingEndpointAdress(camDeviceConnection.getFileDescriptor());
                //mService.libusb_wrapped = true;
                //mService.libusb_InterfacesClaimed = true;
            }
            int bcdUVC_int = 0;
            if (mUsbFs == null) mUsbFs = getUSBFSName(camDevice);
            bcdUVC_int = ((bcdUVC[1] & 0xFF) << 8) | (bcdUVC[0] & 0xFF);
            int lowAndroid = 0;
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                lowAndroid = 1;
            }
            JNA_I_LibUsb.INSTANCE.set_the_native_Values(fd, packetsPerRequest, maxPacketSize, activeUrbs, camStreamingAltSetting, camFormatIndex,
                    camFrameIndex, camFrameInterval, imageWidth, imageHeight, camStreamingEndpointAdress, 1, videoformat,
                    0, bcdUVC_int, lowAndroid);
            //mService.native_values_set=true;
            JNA_I_LibUsb.INSTANCE.initStreamingParms(FD);
        } else {
            // (For transfer buffer sizes > 196608 the kernel file drivers/usb/core/devio.c must be patched.)
            camControlInterface = getVideoControlInterface(camDevice);
            camStreamingInterface = getVideoStreamingInterface(camDevice);
            if (camStreamingInterface.getEndpointCount() < 1) {
                throw new Exception("Streaming interface has no endpoint.");
            }
            camStreamingEndpoint = camStreamingInterface.getEndpoint(0);
            bulkMode = camStreamingEndpoint.getType() == UsbConstants.USB_ENDPOINT_XFER_BULK;
            camDeviceConnection = usbManager.openDevice(camDevice);
            if (camDeviceConnection == null) {
                log("Failed to open the device");
                throw new Exception("Unable to open camera device connection.");
            }
            if (!LIBUSB) {
                if (!camDeviceConnection.claimInterface(camControlInterface, true)) {
                    log("Failed to claim camControlInterface");
                    throw new Exception("Unable to claim camera control interface.");
                }
                if (!camDeviceConnection.claimInterface(camStreamingInterface, true)) {
                    log("Failed to claim camStreamingInterface");
                    throw new Exception("Unable to claim camera streaming interface.");
                }
            }
        }
    }

    public void closeCameraDevice() {

        if (moveToNative) {
            camDeviceConnection = null;
        } else if (camDeviceConnection != null) {
            camDeviceConnection.releaseInterface(camControlInterface);
            camDeviceConnection.releaseInterface(camStreamingInterface);
            camDeviceConnection.close();
            camDeviceConnection = null;
        }
        runningStream = null;
    }

    private void initCamera() throws Exception {
        try {
            getVideoControlErrorCode();  // to reset previous error states
        } catch (Exception e) {
            log("Warning: getVideoControlErrorCode() failed: " + e);
        }   // ignore error, some cameras do not support the request
        try {
            enableStreaming(false);
        } catch (Exception e) {
            //displayErrorMessage(e);
            displayMessage("Warning: enable the Stream failed:\nPlease unplug and replug the camera, or reboot the device");
            log("Warning: enableStreaming(false) failed: " + e);
        }
        try {
            // to reset previous error states
            getVideoStreamErrorCode();
        } catch (Exception e) {
            log("Warning: getVideoStreamErrorCode() failed: " + e);
        }   // ignore error, some cameras do not support the request
        initStreamingParms();
        //initBrightnessParms();
    }

    private void BildaufnahmeButtonClickEvent() {
        imageCapture = true;
        if (LIBUSB) JNA_I_LibUsb.INSTANCE.setImageCapture();
        displayMessage("Image saved");
    }

    //////////////////// Buttons  ///////////////

    public void stopTheCameraStreamClickEvent() {

        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            enableStreaming(false);
        } catch (Exception e) {
            msg = "Plz unplug the camera and replug again, or reboot the device";
            displayMessage("Plz unplug the camera and replug again, or reboot the device");
            e.printStackTrace();
        }
        displayMessage("Stopped ");
        log("Stopped");
        runningStream = null;

    }

    private void writeBytesToFile(String fileName, byte[] data) throws IOException {
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(fileName);
            fileOutputStream.write(data);
            fileOutputStream.flush();
        } finally {
            fileOutputStream.close();
        }
    }

    /// JNI JAVA VM ACCESS METHODS
    public void retrievedStreamActivityFrameFromLibUsb(byte[] streamData) {
        if (videoformat.equals("MJPEG")) {
            try {
                processReceivedMJpegVideoFrameKamera(streamData);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            processReceivedVideoFrameYuvFromJni(streamData);
        }
    }

    public void processReceivedVideoFrameYuvFromJni(byte[] frameData) {
        Videoformat videoFromat;
        if (videoformat.equals("YUV")) {
            videoFromat = Videoformat.YUV;
        } else if (videoformat.equals("YUY2")) {
            videoFromat = Videoformat.YUY2;
        } else if (videoformat.equals("YV12")) {
            videoFromat = Videoformat.YV12;
        } else if (videoformat.equals("YUV_420_888")) {
            videoFromat = Videoformat.YUV_420_888;
        } else if (videoformat.equals("YUV_422_888")) {
            videoFromat = Videoformat.YUV_422_888;
        } else videoFromat = Videoformat.MJPEG;
        try {
            processReceivedVideoFrameYuv(frameData, videoFromat);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void processReceivedVideoFrameYuv(byte[] frameData, Videoformat videoFromat) throws IOException {
        if (videoformat == null) {
            if (videoformat.equals("YUV")) {
                videoFromat = Videoformat.YUV;
            } else if (videoformat.equals("YUY2")) {
                videoFromat = Videoformat.YUY2;
            } else if (videoformat.equals("YV12")) {
                videoFromat = Videoformat.YV12;
            } else if (videoformat.equals("YUV_420_888")) {
                videoFromat = Videoformat.YUV_420_888;
            } else if (videoformat.equals("YUV_422_888")) {
                videoFromat = Videoformat.YUV_422_888;
            } else if (videoformat.equals("UYVY")) {
                videoFromat = Videoformat.UYVY;
            }
        }
        Bitmap bitmap = null;
        if (videoFromat == Videoformat.UYVY) {
            bitmap = Bitmap.createBitmap(imageWidth, imageHeight, Bitmap.Config.ARGB_8888);
            UYVYpixeltobmp(frameData, bitmap, imageWidth, imageHeight);
        } else if (videoFromat == Videoformat.YUY2) {
            bitmap = Bitmap.createBitmap(imageWidth, imageHeight, Bitmap.Config.ARGB_8888);
            YUY2pixeltobmp(frameData, bitmap, imageWidth, imageHeight);
        } else {
            YuvImage yuvImage = null;
            if (videoFromat == Videoformat.YUY2) {
                yuvImage = new YuvImage(frameData, ImageFormat.YUY2, imageWidth, imageHeight, null);
            } else if (videoFromat == Videoformat.YV12) {
                yuvImage = new YuvImage(frameData, ImageFormat.YV12, imageWidth, imageHeight, null);
            } else if (videoFromat == Videoformat.YUV_420_888) {
                yuvImage = new YuvImage(frameData, ImageFormat.YUV_420_888, imageWidth, imageHeight, null);
            } else if (videoFromat == Videoformat.YUV_422_888) {
                yuvImage = new YuvImage(frameData, ImageFormat.YUV_422_888, imageWidth, imageHeight, null);
            } else if (videoFromat == Videoformat.NV21) {
                yuvImage = new YuvImage(frameData, ImageFormat.NV21, imageWidth, imageHeight, null);
            } else if (videoFromat == Videoformat.UYVY) {

            } else {
                yuvImage = new YuvImage(frameData, ImageFormat.YUY2, imageWidth, imageHeight, null);
            }
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            yuvImage.compressToJpeg(new Rect(0, 0, imageWidth, imageHeight), 100, os);
            byte[] jpegByteArray;
            jpegByteArray = os.toByteArray();
            bitmap = BitmapFactory.decodeByteArray(jpegByteArray, 0, jpegByteArray.length);
        }

        if (exit == false) {
            // Image
            //final Bitmap bitmap = BitmapFactory.decodeByteArray(jpegByteArray, 0, jpegByteArray.length);
            final Bitmap bmp = bitmap;
            Bitmap grayBmp = toGrayscale(bmp);
            // Bitmap bmpRev = revervseBitmap(grayBmp);

            // Temperature
            LEP_RAD_SPOTMETER_OBJ_KELVIN_T kelvinObj = getRadSpotmeterObjInKelvinX100();
            if (kelvinObj != null) {
                maxTemperature = kevlin2Celsius(kelvinObj.radSpotmeterMaxValue);
                minTemperature = kevlin2Celsius(kelvinObj.radSpotmeterMinValue);
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    textView.setText(String.format("Max Temperature: %.2f", maxTemperature));
                    imageView.setImageBitmap(bmp);
                    textView2.setText(String.format("Min Temperature:%.2f", minTemperature));
                    imageView2.setImageBitmap(grayBmp);
                }
            });

            if (videorecordApiJellyBeanNup) {
                bitmapToVideoEncoder.queueFrame(bitmap);
            }
        }
    }

    private LEP_RAD_SPOTMETER_OBJ_KELVIN_T getRadSpotmeterObjInKelvinX100() {
        int timeout = 0;
        int attributeWordLength = 4;
        byte[] valueParams = new byte[attributeWordLength * 2];
        int commandID = LEP_CID_RAD_SPOTMETER_OBJ_KELVIN;
        int unitID = leptonCommandIdToUnitId(commandID);
        int controlID = ((commandID & 0x00ff) >> 2) + 1;
        int len;
        len = camDeviceConnection.controlTransfer(RT_CLASS_INTERFACE_GET, GET_CUR, (short) (controlID << 8), (unitID << 8) | camControlInterface.getId(), valueParams, valueParams.length, timeout);
        if (len != valueParams.length) {
            Log.d("getTemperature", "Get Rad Spotmeter object in Kelvin x 100 failure.");
            return null;
        } else {
            LEP_RAD_SPOTMETER_OBJ_KELVIN_T obj = new LEP_RAD_SPOTMETER_OBJ_KELVIN_T();
            obj.radSpotmeterValue = (int) (((valueParams[1] & 0xff) << 8) | valueParams[0] & 0xff);
            obj.radSpotmeterMaxValue = (int) (((valueParams[3] & 0xff) << 8) | valueParams[2]& 0xff);
            obj.radSpotmeterMinValue = (int) (((valueParams[5] & 0xff) << 8) | valueParams[4] & 0xff);
            obj.radSpotmeterPopulation = (int) (((valueParams[7] & 0xff) << 8) | valueParams[6] & 0xff);
            return obj;
        }
    }

    private int leptonCommandIdToUnitId(int commandID) {
        int unit_id;

        switch (commandID & 0x3f00) // Ignore upper 2 bits including OEM bit
        {
            case LEP_CID_AGC_MODULE:
                unit_id = VC_CONTROL_XU_LEP_AGC_ID;
                break;

            case LEP_CID_OEM_MODULE:
                unit_id = VC_CONTROL_XU_LEP_OEM_ID;
                break;

            case LEP_CID_RAD_MODULE:
                unit_id = VC_CONTROL_XU_LEP_RAD_ID;
                break;

            case LEP_CID_SYS_MODULE:
                unit_id = VC_CONTROL_XU_LEP_SYS_ID;
                break;

            case LEP_CID_VID_MODULE:
                unit_id = VC_CONTROL_XU_LEP_VID_ID;
                break;

            default:
                return -3; // LEPTON_ErrorCodes Camera range error
        }

        return unit_id;
    }


    private double kevlin2Celsius(int kevlin) {
        return (kevlin - 27315) / 100.0;
    }

    private double grayscale2temp(int pixel, double maxTemp, double minTemp) {
        return (minTemp + (maxTemp - minTemp)) * pixel / 255;
    }


    private void printBitmapPixel(Bitmap bmp) {
        Log.d("Bitmap", String.format("Bitmap: (%d x %d)", bmp.getWidth(), bmp.getHeight()));
        StringBuilder sb = new StringBuilder();
        for (int h = 0; h < bmp.getHeight(); h++) {
            for (int w = 0; w < bmp.getWidth(); w++) {
                sb.append(String.format("0x%08x ", bmp.getPixel(w, h)));
            }
            sb.append("\n");
        }
        Log.d("Bitmap", "\n" + sb.toString() + "\n");
    }

    private Bitmap toGrayscale(Bitmap bmpOriginal) {
        int width, height;
        height = bmpOriginal.getHeight();
        width = bmpOriginal.getWidth();

        Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bmpGrayscale);
        Paint paint = new Paint();
        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0);
        ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
        paint.setColorFilter(f);
        c.drawBitmap(bmpOriginal, 0, 0, paint);
        return bmpGrayscale;
    }

    private Bitmap revervseBitmap(Bitmap bmpOriginal) {
        int width, height;
        height = bmpOriginal.getHeight();
        width = bmpOriginal.getWidth();

        final int[] pixels = new int[bmpOriginal.getHeight() * bmpOriginal.getWidth()];
        bmpOriginal.getPixels(pixels, 0, bmpOriginal.getWidth(), 0, 0,
                bmpOriginal.getWidth(), bmpOriginal.getHeight());

        for (int i = 0; i < pixels.length; i += 1) {
            int pixel = pixels[i];
            pixels[i] = Color.argb(
                    Color.alpha(pixel),
                    Color.red(pixel) ^ 0xff,
                    Color.blue(pixel) ^ 0xff,
                    Color.green(pixel) ^ 0xff);

        }

        Bitmap bmpRev = Bitmap.createBitmap(bmpOriginal.getWidth(), bmpOriginal.getHeight(), bmpOriginal.getConfig());
        bmpRev.setPixels(pixels, 0, bmpOriginal.getWidth(), 0, 0, bmpOriginal.getWidth(), bmpOriginal.getHeight());
        return bmpRev;
    }

    private double KelvinToCelsius(double kelvin) {
        return kelvin / 100.0 - 273.15;
    }

    public Bitmap flipImage(Bitmap src) {
        // create new matrix for transformation
        Matrix matrix = new Matrix();
        if (horizontalFlip) matrix.preScale(1.0f, -1.0f);
        if (verticalFlip) matrix.preScale(-1.0f, 1.0f);
        if (rotate != 0) matrix.postRotate(rotate);
        // return transformed image
        return Bitmap.createBitmap(src, 0, 0, src.getWidth(), src.getHeight(), matrix, true);
    }

    public void processReceivedMJpegVideoFrameKamera(byte[] mjpegFrameData) throws Exception {
        byte[] jpegFrameData = convertMjpegFrameToJpegKamera(mjpegFrameData);
        if (imageCapture) {
            imageCapture = false;
            date = new Date();
            dateFormat = new SimpleDateFormat("dd.MM.yyyy___HH_mm_ss");
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.O_MR1) {
                String rootPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/UVC_Camera/Pictures/";
                file = new File(rootPath);
                if (!file.exists()) {
                    file.mkdirs();
                }
                String fileName = new File(rootPath + dateFormat.format(date) + ".jpg").getAbsolutePath();
                try {
                    writeBytesToFile(fileName, jpegFrameData);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                log("file saved");
            } else {
                Context context = getApplicationContext();
                String fileName = new File(dateFormat.format(date) + ".JPG").getPath();
                log("fileName = " + fileName);
                try {
                    final Bitmap bitmap = BitmapFactory.decodeByteArray(jpegFrameData, 0, jpegFrameData.length);
                    MediaStore.Images.Media.insertImage(context.getContentResolver(), bitmap, fileName, "Usb_Camera_Picture");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                log("file saved");
            }
        }
        if (saveStillImage) {
            saveStillImage = false;
            date = new Date();
            dateFormat = new SimpleDateFormat("\"dd.MM.yyyy___HH_mm_ss");

            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.O_MR1) {
                String rootPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/UVC_Camera/Pictures/";
                file = new File(rootPath);
                if (!file.exists()) {
                    file.mkdirs();
                }
                String fileName = new File(rootPath + dateFormat.format(date) + ".jpg").getAbsolutePath();
                try {
                    writeBytesToFile(fileName, jpegFrameData);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                log("Still Image Save Complete");
            } else {
                Context context = getApplicationContext();
                String fileName = new File(dateFormat.format(date) + ".JPG").getPath();
                log("fileName = " + fileName);
                try {
                    final Bitmap bitmap = BitmapFactory.decodeByteArray(jpegFrameData, 0, jpegFrameData.length);
                    MediaStore.Images.Media.insertImage(context.getContentResolver(), bitmap, fileName, "Usb_Camera_Picture");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                log("Still Image Save Complete");
            }
        }
        if (videorecord) {
            if (System.currentTimeMillis() - currentTime > 200) {
                currentTime = System.currentTimeMillis();
                lastPicture++;
                String dirname = "Video";
                File dir = new File(getExternalFilesDir(null), dirname);
                if (!dir.exists()) {
                    dir.mkdirs();
                }
                File sub_dir = new File(dir, "rec");
                if (!sub_dir.exists()) {
                    sub_dir.mkdirs();
                }
                String fileName = new File(sub_dir, lastPicture + ".JPG").getPath();
                writeBytesToFile(fileName, jpegFrameData);
            }
        }
        if (exit == false) {
            if (lowerResolution) {
                BitmapFactory.Options opts = new BitmapFactory.Options();
                opts.inSampleSize = 4;
                final Bitmap bitmap = BitmapFactory.decodeByteArray(jpegFrameData, 0, jpegFrameData.length, opts);
                //Bitmap bitmap = decodeSampledBitmapFromByteArray(jpegFrameData);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        imageView.setImageBitmap(bitmap);
                    }
                });
                if (videorecordApiJellyBeanNup) {
                    bitmapToVideoEncoder.queueFrame(bitmap);
                }
            } else {
                final Bitmap bitmap = BitmapFactory.decodeByteArray(jpegFrameData, 0, jpegFrameData.length);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (horizontalFlip || verticalFlip || rotate != 0) {
                            imageView.setImageBitmap(flipImage(bitmap));
                        } else imageView.setImageBitmap(bitmap);
                    }
                });
                if (videorecordApiJellyBeanNup) {
                    bitmapToVideoEncoder.queueFrame(bitmap);
                }
            }
        }
    }

    // see USB video class standard, USB_Video_Payload_MJPEG_1.5.pdf
    private byte[] convertMjpegFrameToJpegKamera(byte[] frameData) throws Exception {
        int frameLen = frameData.length;
        while (frameLen > 0 && frameData[frameLen - 1] == 0) {
            frameLen--;
        }
        if (frameLen < 100 || (frameData[0] & 0xff) != 0xff || (frameData[1] & 0xff) != 0xD8 || (frameData[frameLen - 2] & 0xff) != 0xff || (frameData[frameLen - 1] & 0xff) != 0xd9) {
            logError("Invalid MJPEG frame structure, length= " + frameData.length);
        }
        boolean hasHuffmanTable = findJpegSegment(frameData, frameLen, 0xC4) != -1;
        exit = false;
        if (hasHuffmanTable) {
            if (frameData.length == frameLen) {
                return frameData;
            }
            return Arrays.copyOf(frameData, frameLen);
        } else {
            int segmentDaPos = findJpegSegment(frameData, frameLen, 0xDA);

            try {
                if (segmentDaPos == -1) exit = true;
            } catch (Exception e) {
                logError("Segment 0xDA not found in MJPEG frame data.");
            }
            //          throw new Exception("Segment 0xDA not found in MJPEG frame data.");
            if (exit == false) {
                byte[] a = new byte[frameLen + mjpgHuffmanTable.length];
                System.arraycopy(frameData, 0, a, 0, segmentDaPos);
                System.arraycopy(mjpgHuffmanTable, 0, a, segmentDaPos, mjpgHuffmanTable.length);
                System.arraycopy(frameData, segmentDaPos, a, segmentDaPos + mjpgHuffmanTable.length, frameLen - segmentDaPos);
                return a;
            } else
                return null;
        }
    }

    private int findJpegSegment(byte[] a, int dataLen, int segmentType) {
        int p = 2;
        while (p <= dataLen - 6) {
            if ((a[p] & 0xff) != 0xff) {
                log("Unexpected JPEG data structure (marker expected).");
                break;
            }
            int markerCode = a[p + 1] & 0xff;
            if (markerCode == segmentType) {
                return p;
            }
            if (markerCode >= 0xD0 && markerCode <= 0xDA) {       // stop when scan data begins
                break;
            }
            int len = ((a[p + 2] & 0xff) << 8) + (a[p + 3] & 0xff);
            p += len + 2;
        }
        return -1;
    }

    private void initStreamingParms() throws Exception {
        stringBuilder = new StringBuilder();
        final int timeout = 5000;
        int usedStreamingParmsLen;
        int len;
        byte[] streamingParms = new byte[26];
        // The e-com module produces errors with 48 bytes (UVC 1.5) instead of 26 bytes (UVC 1.1) streaming parameters! We could use the USB version info to determine the size of the streaming parameters.
        streamingParms[0] = (byte) 0x01;                // (0x01: dwFrameInterval) //D0: dwFrameInterval //D1: wKeyFrameRate // D2: wPFrameRate // D3: wCompQuality // D4: wCompWindowSize
        streamingParms[2] = (byte) camFormatIndex;                // bFormatIndex
        streamingParms[3] = (byte) camFrameIndex;                 // bFrameIndex
        packUsbInt(camFrameInterval, streamingParms, 4);         // dwFrameInterval
        log("Initial streaming parms: " + dumpStreamingParms(streamingParms));
        stringBuilder.append("Initial streaming parms: \n");
        stringBuilder.append(dumpStreamingParms(streamingParms));
        len = camDeviceConnection.controlTransfer(RT_CLASS_INTERFACE_SET, SET_CUR, VS_PROBE_CONTROL << 8, camStreamingInterface.getId(), streamingParms, streamingParms.length, timeout);
        if (len != streamingParms.length) {
            throw new Exception("Camera initialization failed. Streaming parms probe set failed, len=" + len + ".");
        }
        // for (int i = 0; i < streamingParms.length; i++) streamingParms[i] = 99;          // temp test
        len = camDeviceConnection.controlTransfer(RT_CLASS_INTERFACE_GET, GET_CUR, VS_PROBE_CONTROL << 8, camStreamingInterface.getId(), streamingParms, streamingParms.length, timeout);
        if (len != streamingParms.length) {
            throw new Exception("Camera initialization failed. Streaming parms probe get failed.");
        }
        log("Probed streaming parms: " + dumpStreamingParms(streamingParms));
        stringBuilder.append("\nProbed streaming parms:  \n");
        stringBuilder.append(dumpStreamingParms(streamingParms));
        usedStreamingParmsLen = len;
        // log("Streaming parms length: " + usedStreamingParmsLen);
        len = camDeviceConnection.controlTransfer(RT_CLASS_INTERFACE_SET, SET_CUR, VS_COMMIT_CONTROL << 8, camStreamingInterface.getId(), streamingParms, usedStreamingParmsLen, timeout);
        if (len != streamingParms.length) {
            throw new Exception("Camera initialization failed. Streaming parms commit set failed.");
        }
        // for (int i = 0; i < streamingParms.length; i++) streamingParms[i] = 99;          // temp test
        len = camDeviceConnection.controlTransfer(RT_CLASS_INTERFACE_GET, GET_CUR, VS_COMMIT_CONTROL << 8, camStreamingInterface.getId(), streamingParms, usedStreamingParmsLen, timeout);
        if (len != streamingParms.length) {
            log("Final streaming parms receive failed");
            //throw new Exception("Camera initialization failed. Streaming parms commit get failed.");
        }
        log("Final streaming parms: " + dumpStreamingParms(streamingParms));
        stringBuilder.append("\nFinal streaming parms: \n");
        stringBuilder.append(dumpStreamingParms(streamingParms));
        controlltransfer = new String(dumpStreamingParms(streamingParms));
    }

    private String dumpStreamingParms(byte[] p) {
        StringBuilder s = new StringBuilder(128);
        s.append("hint=0x" + Integer.toHexString(unpackUsbUInt2(p, 0)));
        s.append(" format=" + (p[2] & 0xf));
        s.append(" frame=" + (p[3] & 0xf));
        s.append(" frameInterval=" + unpackUsbInt(p, 4));
        s.append(" keyFrameRate=" + unpackUsbUInt2(p, 8));
        s.append(" pFrameRate=" + unpackUsbUInt2(p, 10));
        s.append(" compQuality=" + unpackUsbUInt2(p, 12));
        s.append(" compWindowSize=" + unpackUsbUInt2(p, 14));
        s.append(" delay=" + unpackUsbUInt2(p, 16));
        s.append(" maxVideoFrameSize=" + unpackUsbInt(p, 18));
        s.append(" maxPayloadTransferSize=" + unpackUsbInt(p, 22));
        return s.toString();
    }

    private void initStillImageParms() {
        final int timeout = 5000;
        int len;
        byte[] parms = new byte[11];
        if (moveToNative) return;
        len = camDeviceConnection.controlTransfer(RT_CLASS_INTERFACE_GET, GET_MIN, VS_STILL_PROBE_CONTROL << 8, camStreamingInterface.getId(), parms, parms.length, timeout);
        if (len != parms.length) {
            log("Camera initialization failed. Still image parms probe get failed.");
        }
        log("Probed still image parms (GET_MIN): " + dumpStillImageParms(parms));
        len = camDeviceConnection.controlTransfer(RT_CLASS_INTERFACE_GET, GET_MAX, VS_STILL_PROBE_CONTROL << 8, camStreamingInterface.getId(), parms, parms.length, timeout);
        if (len != parms.length) {
            log("Camera initialization failed. Still image parms probe get failed.");
        }
        log("Probed still image parms (GET_MAX): " + dumpStillImageParms(parms));
        parms[0] = (byte) camFormatIndex;
        parms[1] = (byte) camFrameIndex;
        //parms[2] = 1;

        len = camDeviceConnection.controlTransfer(RT_CLASS_INTERFACE_GET, GET_CUR, VS_STILL_PROBE_CONTROL << 8, camStreamingInterface.getId(), parms, parms.length, timeout);
        if (len != parms.length) {
            log("Camera initialization failed. Still image parms probe get failed.");
        }
        log("Probed still image parms (GET_CUR): " + dumpStillImageParms(parms));
        parms[0] = (byte) camFormatIndex;
        parms[1] = (byte) camFrameIndex;
        len = camDeviceConnection.controlTransfer(RT_CLASS_INTERFACE_SET, SET_CUR, VS_STILL_COMMIT_CONTROL << 8, camStreamingInterface.getId(), parms, parms.length, timeout);
        if (len != parms.length) {
            log("Camera initialization failed. Still image parms commit set failed.");
        }
        len = camDeviceConnection.controlTransfer(RT_CLASS_INTERFACE_GET, GET_CUR, VS_STILL_COMMIT_CONTROL << 8, camStreamingInterface.getId(), parms, parms.length, timeout);
        if (len != parms.length) {
            log("Camera initialization failed. Still image parms commit get failed. len=" + len);
        }
        log("Final still image parms: " + dumpStillImageParms(parms));
    }

    private String dumpStillImageParms(byte[] p) {
        StringBuilder s = new StringBuilder(128);
        s.append("bFormatIndex=" + (p[0] & 0xff));
        s.append(" bFrameIndex=" + (p[1] & 0xff));
        s.append(" bCompressionIndex=" + (p[2] & 0xff));
        s.append(" maxVideoFrameSize=" + unpackUsbInt(p, 3));
        s.append(" maxPayloadTransferSize=" + unpackUsbInt(p, 7));
        return s.toString();
    }

    private static int unpackUsbInt(byte[] buf, int pos) {
        return unpackInt(buf, pos, false);
    }

    private static int unpackUsbUInt2(byte[] buf, int pos) {
        return ((buf[pos + 1] & 0xFF) << 8) | (buf[pos] & 0xFF);
    }

    private static void packUsbInt(int i, byte[] buf, int pos) {
        packInt(i, buf, pos, false);
    }

    private static void packInt(int i, byte[] buf, int pos, boolean bigEndian) {
        if (bigEndian) {
            buf[pos] = (byte) ((i >>> 24) & 0xFF);
            buf[pos + 1] = (byte) ((i >>> 16) & 0xFF);
            buf[pos + 2] = (byte) ((i >>> 8) & 0xFF);
            buf[pos + 3] = (byte) (i & 0xFF);
        } else {
            buf[pos] = (byte) (i & 0xFF);
            buf[pos + 1] = (byte) ((i >>> 8) & 0xFF);
            buf[pos + 2] = (byte) ((i >>> 16) & 0xFF);
            buf[pos + 3] = (byte) ((i >>> 24) & 0xFF);
        }
    }

    private static int unpackInt(byte[] buf, int pos, boolean bigEndian) {
        if (bigEndian) {
            return (buf[pos] << 24) | ((buf[pos + 1] & 0xFF) << 16) | ((buf[pos + 2] & 0xFF) << 8) | (buf[pos + 3] & 0xFF);
        } else {
            return (buf[pos + 3] << 24) | ((buf[pos + 2] & 0xFF) << 16) | ((buf[pos + 1] & 0xFF) << 8) | (buf[pos] & 0xFF);
        }
    }

    private void enableStreaming(boolean enabled) throws Exception {
        enableStreaming_usbFs(enabled);
    }

    private void enableStreaming_usbFs(boolean enabled) throws Exception {
        if (enabled && bulkMode) {
            // clearHalt(camStreamingEndpoint.getAddress());
        }
        int altSetting = enabled ? camStreamingAltSetting : 0;
        // For bulk endpoints, altSetting is always 0.
        log("setAltSetting");
        usbdevice_fs_util.setInterface(camDeviceConnection.getFileDescriptor(), camStreamingInterface.getId(), altSetting);
    }

    private void sendStillImageTrigger() {
        log("Sending Still Image Trigger");
        byte buf[] = new byte[1];
        buf[0] = 1;
        int len = camDeviceConnection.controlTransfer(RT_CLASS_INTERFACE_SET, SET_CUR, VS_STILL_IMAGE_TRIGGER_CONTROL << 8, camStreamingInterface.getId(), buf, 1, 1000);
        if (len != 1) {
            displayMessage("Still Image Controltransfer failed !!");
            log("Still Image Controltransfer failed !!");
        }
    }

    // Resets the error code after retrieving it.
    private int getVideoControlErrorCode() throws Exception {
        byte buf[] = new byte[1];
        buf[0] = 99;
        int len = camDeviceConnection.controlTransfer(RT_CLASS_INTERFACE_GET, GET_CUR, VC_REQUEST_ERROR_CODE_CONTROL << 8, 0, buf, 1, 1000);
        if (len != 1) {
            throw new Exception("VC_REQUEST_ERROR_CODE_CONTROL failed, len=" + len + ".");
        }
        return buf[0];
    }

    // Does not work with Logitech C310? Always returns 0.
    private int getVideoStreamErrorCode() throws Exception {
        byte buf[] = new byte[1];
        buf[0] = 99;
        int len = camDeviceConnection.controlTransfer(RT_CLASS_INTERFACE_GET, GET_CUR, VS_STREAM_ERROR_CODE_CONTROL << 8, camStreamingInterface.getId(), buf, 1, 1000);
        if (len == 0) {
            return 0;
        }                   // ? (Logitech C310 returns len=0)
        if (len != 1) {
            throw new Exception("VS_STREAM_ERROR_CODE_CONTROL failed, len=" + len + ".");
        }
        return buf[0];
    }

    public void displayMessage(final String msg) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(StartIsoStreamActivity.this, msg, Toast.LENGTH_LONG).show();
            }
        });
    }

    public void log(String msg) {
        Log.i("UVC_Camera_Iso_Stream", msg);
    }

    public void logError(String msg) {
        Log.e("UVC_Camera", msg);
    }

    public void displayErrorMessage(Throwable e) {
        Log.e("UVC_Camera", "Error in MainActivity", e);
        displayMessage("Error: " + e);
    }

    // see 10918-1:1994, K.3.3.1 Specification of typical tables for DC difference coding
    private static byte[] mjpgHuffmanTable = {
            (byte) 0xff, (byte) 0xc4, (byte) 0x01, (byte) 0xa2, (byte) 0x00, (byte) 0x00, (byte) 0x01, (byte) 0x05, (byte) 0x01, (byte) 0x01,
            (byte) 0x01, (byte) 0x01, (byte) 0x01, (byte) 0x01, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00,
            (byte) 0x00, (byte) 0x00, (byte) 0x01, (byte) 0x02, (byte) 0x03, (byte) 0x04, (byte) 0x05, (byte) 0x06, (byte) 0x07, (byte) 0x08,
            (byte) 0x09, (byte) 0x0a, (byte) 0x0b, (byte) 0x10, (byte) 0x00, (byte) 0x02, (byte) 0x01, (byte) 0x03, (byte) 0x03, (byte) 0x02,
            (byte) 0x04, (byte) 0x03, (byte) 0x05, (byte) 0x05, (byte) 0x04, (byte) 0x04, (byte) 0x00, (byte) 0x00, (byte) 0x01, (byte) 0x7d,
            (byte) 0x01, (byte) 0x02, (byte) 0x03, (byte) 0x00, (byte) 0x04, (byte) 0x11, (byte) 0x05, (byte) 0x12, (byte) 0x21, (byte) 0x31,
            (byte) 0x41, (byte) 0x06, (byte) 0x13, (byte) 0x51, (byte) 0x61, (byte) 0x07, (byte) 0x22, (byte) 0x71, (byte) 0x14, (byte) 0x32,
            (byte) 0x81, (byte) 0x91, (byte) 0xa1, (byte) 0x08, (byte) 0x23, (byte) 0x42, (byte) 0xb1, (byte) 0xc1, (byte) 0x15, (byte) 0x52,
            (byte) 0xd1, (byte) 0xf0, (byte) 0x24, (byte) 0x33, (byte) 0x62, (byte) 0x72, (byte) 0x82, (byte) 0x09, (byte) 0x0a, (byte) 0x16,
            (byte) 0x17, (byte) 0x18, (byte) 0x19, (byte) 0x1a, (byte) 0x25, (byte) 0x26, (byte) 0x27, (byte) 0x28, (byte) 0x29, (byte) 0x2a,
            (byte) 0x34, (byte) 0x35, (byte) 0x36, (byte) 0x37, (byte) 0x38, (byte) 0x39, (byte) 0x3a, (byte) 0x43, (byte) 0x44, (byte) 0x45,
            (byte) 0x46, (byte) 0x47, (byte) 0x48, (byte) 0x49, (byte) 0x4a, (byte) 0x53, (byte) 0x54, (byte) 0x55, (byte) 0x56, (byte) 0x57,
            (byte) 0x58, (byte) 0x59, (byte) 0x5a, (byte) 0x63, (byte) 0x64, (byte) 0x65, (byte) 0x66, (byte) 0x67, (byte) 0x68, (byte) 0x69,
            (byte) 0x6a, (byte) 0x73, (byte) 0x74, (byte) 0x75, (byte) 0x76, (byte) 0x77, (byte) 0x78, (byte) 0x79, (byte) 0x7a, (byte) 0x83,
            (byte) 0x84, (byte) 0x85, (byte) 0x86, (byte) 0x87, (byte) 0x88, (byte) 0x89, (byte) 0x8a, (byte) 0x92, (byte) 0x93, (byte) 0x94,
            (byte) 0x95, (byte) 0x96, (byte) 0x97, (byte) 0x98, (byte) 0x99, (byte) 0x9a, (byte) 0xa2, (byte) 0xa3, (byte) 0xa4, (byte) 0xa5,
            (byte) 0xa6, (byte) 0xa7, (byte) 0xa8, (byte) 0xa9, (byte) 0xaa, (byte) 0xb2, (byte) 0xb3, (byte) 0xb4, (byte) 0xb5, (byte) 0xb6,
            (byte) 0xb7, (byte) 0xb8, (byte) 0xb9, (byte) 0xba, (byte) 0xc2, (byte) 0xc3, (byte) 0xc4, (byte) 0xc5, (byte) 0xc6, (byte) 0xc7,
            (byte) 0xc8, (byte) 0xc9, (byte) 0xca, (byte) 0xd2, (byte) 0xd3, (byte) 0xd4, (byte) 0xd5, (byte) 0xd6, (byte) 0xd7, (byte) 0xd8,
            (byte) 0xd9, (byte) 0xda, (byte) 0xe1, (byte) 0xe2, (byte) 0xe3, (byte) 0xe4, (byte) 0xe5, (byte) 0xe6, (byte) 0xe7, (byte) 0xe8,
            (byte) 0xe9, (byte) 0xea, (byte) 0xf1, (byte) 0xf2, (byte) 0xf3, (byte) 0xf4, (byte) 0xf5, (byte) 0xf6, (byte) 0xf7, (byte) 0xf8,
            (byte) 0xf9, (byte) 0xfa, (byte) 0x01, (byte) 0x00, (byte) 0x03, (byte) 0x01, (byte) 0x01, (byte) 0x01, (byte) 0x01, (byte) 0x01,
            (byte) 0x01, (byte) 0x01, (byte) 0x01, (byte) 0x01, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00,
            (byte) 0x01, (byte) 0x02, (byte) 0x03, (byte) 0x04, (byte) 0x05, (byte) 0x06, (byte) 0x07, (byte) 0x08, (byte) 0x09, (byte) 0x0a,
            (byte) 0x0b, (byte) 0x11, (byte) 0x00, (byte) 0x02, (byte) 0x01, (byte) 0x02, (byte) 0x04, (byte) 0x04, (byte) 0x03, (byte) 0x04,
            (byte) 0x07, (byte) 0x05, (byte) 0x04, (byte) 0x04, (byte) 0x00, (byte) 0x01, (byte) 0x02, (byte) 0x77, (byte) 0x00, (byte) 0x01,
            (byte) 0x02, (byte) 0x03, (byte) 0x11, (byte) 0x04, (byte) 0x05, (byte) 0x21, (byte) 0x31, (byte) 0x06, (byte) 0x12, (byte) 0x41,
            (byte) 0x51, (byte) 0x07, (byte) 0x61, (byte) 0x71, (byte) 0x13, (byte) 0x22, (byte) 0x32, (byte) 0x81, (byte) 0x08, (byte) 0x14,
            (byte) 0x42, (byte) 0x91, (byte) 0xa1, (byte) 0xb1, (byte) 0xc1, (byte) 0x09, (byte) 0x23, (byte) 0x33, (byte) 0x52, (byte) 0xf0,
            (byte) 0x15, (byte) 0x62, (byte) 0x72, (byte) 0xd1, (byte) 0x0a, (byte) 0x16, (byte) 0x24, (byte) 0x34, (byte) 0xe1, (byte) 0x25,
            (byte) 0xf1, (byte) 0x17, (byte) 0x18, (byte) 0x19, (byte) 0x1a, (byte) 0x26, (byte) 0x27, (byte) 0x28, (byte) 0x29, (byte) 0x2a,
            (byte) 0x35, (byte) 0x36, (byte) 0x37, (byte) 0x38, (byte) 0x39, (byte) 0x3a, (byte) 0x43, (byte) 0x44, (byte) 0x45, (byte) 0x46,
            (byte) 0x47, (byte) 0x48, (byte) 0x49, (byte) 0x4a, (byte) 0x53, (byte) 0x54, (byte) 0x55, (byte) 0x56, (byte) 0x57, (byte) 0x58,
            (byte) 0x59, (byte) 0x5a, (byte) 0x63, (byte) 0x64, (byte) 0x65, (byte) 0x66, (byte) 0x67, (byte) 0x68, (byte) 0x69, (byte) 0x6a,
            (byte) 0x73, (byte) 0x74, (byte) 0x75, (byte) 0x76, (byte) 0x77, (byte) 0x78, (byte) 0x79, (byte) 0x7a, (byte) 0x82, (byte) 0x83,
            (byte) 0x84, (byte) 0x85, (byte) 0x86, (byte) 0x87, (byte) 0x88, (byte) 0x89, (byte) 0x8a, (byte) 0x92, (byte) 0x93, (byte) 0x94,
            (byte) 0x95, (byte) 0x96, (byte) 0x97, (byte) 0x98, (byte) 0x99, (byte) 0x9a, (byte) 0xa2, (byte) 0xa3, (byte) 0xa4, (byte) 0xa5,
            (byte) 0xa6, (byte) 0xa7, (byte) 0xa8, (byte) 0xa9, (byte) 0xaa, (byte) 0xb2, (byte) 0xb3, (byte) 0xb4, (byte) 0xb5, (byte) 0xb6,
            (byte) 0xb7, (byte) 0xb8, (byte) 0xb9, (byte) 0xba, (byte) 0xc2, (byte) 0xc3, (byte) 0xc4, (byte) 0xc5, (byte) 0xc6, (byte) 0xc7,
            (byte) 0xc8, (byte) 0xc9, (byte) 0xca, (byte) 0xd2, (byte) 0xd3, (byte) 0xd4, (byte) 0xd5, (byte) 0xd6, (byte) 0xd7, (byte) 0xd8,
            (byte) 0xd9, (byte) 0xda, (byte) 0xe2, (byte) 0xe3, (byte) 0xe4, (byte) 0xe5, (byte) 0xe6, (byte) 0xe7, (byte) 0xe8, (byte) 0xe9,
            (byte) 0xea, (byte) 0xf2, (byte) 0xf3, (byte) 0xf4, (byte) 0xf5, (byte) 0xf6, (byte) 0xf7, (byte) 0xf8, (byte) 0xf9, (byte) 0xfa};


    public class IsochronousStream extends Thread {
        private Activity activity;
        private boolean reapTheLastFrames;
        private int lastReapedFrames = 0;

        public IsochronousStream(Context mContext) {
            setPriority(Thread.MAX_PRIORITY);
            activity = (Activity) mContext;
        }

        private boolean write = false;
        private int framecnt = 0;

        // Main: Get frame data
        public void run() {
            try {
                USBIso usbIso64 = new USBIso(camDeviceConnection.getFileDescriptor(), packetsPerRequest, maxPacketSize, (byte) camStreamingEndpoint.getAddress());
                usbIso64.preallocateRequests(activeUrbs);
                ByteArrayOutputStream frameData = new ByteArrayOutputStream(0x20000);
                int skipFrames = 0;
                byte[] data = new byte[maxPacketSize];
                enableStreaming(true);
                usbIso64.submitUrbs();
                while (true) {
                    if (pauseCamera) {
                        Thread.sleep(200);
                    } else {
                        USBIso.Request req = usbIso64.reapRequest(true);
                        for (int packetNo = 0; packetNo < req.getNumberOfPackets(); packetNo++) {
                            int packetStatus = req.getPacketStatus(packetNo);
                            try {
                                if (packetStatus != 0) {
                                    skipFrames = 1;
                                }
                            } catch (Exception e) {
                                log("Camera read error, packet status=" + packetStatus);
                            }
                            int packetLen = req.getPacketActualLength(packetNo);
                            if (packetLen == 0) {
                                continue;
                            }
                            if (packetLen > maxPacketSize) {
                                throw new Exception("packetLen > maxPacketSize");
                            }
                            req.getPacketData(packetNo, data, packetLen);
                            int headerLen = data[0] & 0xff;

                            try {
                                if (headerLen < 2 || headerLen > packetLen) {
                                    skipFrames = 1;
                                }
                            } catch (Exception e) {
                                log("Invalid payload header length.");
                            }
                            int headerFlags = data[1] & 0xff;
                            int dataLen = packetLen - headerLen;
                            boolean error = (headerFlags & 0x40) != 0;
                            if (error && skipFrames == 0) skipFrames = 1;
                            if (dataLen > 0 && skipFrames == 0) {
                                frameData.write(data, headerLen, dataLen);
                            }
                            if ((headerFlags & 2) != 0) {
                                if (frameData.size() <= 6000) skipFrames = 1;
                                // check Frame Size
                                if (checkFrameSize(frameData.size())) {
                                    skipFrames = 1;
                                    log("Checking Frame --> Skip Retruned");
                                } else if (frameData.size() > (imageWidth * imageHeight * 2)) {
                                    log("Framesize > Max YUY2 FrameSize");
                                    skipFrames = 1;
                                }
                                if (skipFrames > 0) {
                                    log("Skipping frame, len= " + frameData.size());
                                    frameData.reset();
                                    skipFrames--;
                                } else {
                                    log("Frame process, len= " + frameData.size());

                                    if (stillImageAufnahme) {
                                        sendStillImageTrigger();
                                        stillImageAufnahme = false;
                                    }
                                    if ((headerFlags & 0x20) != 0) {
                                        log("Still Image Bit set.\nSetting saveStillImage");
                                        saveStillImage = true;

                                    }

                                    /* Get frame origin raw data */
                                    frameData.write(data, headerLen, dataLen);
//                                    Log.d("frameData", "Length = " + frameData.toByteArray().length);
//                                    if (frameData.size() == (imageWidth * imageHeight * 2)) {
//                                        Log.d("frameData detail", "Header len:" + headerLen);
//                                        Log.d("frameData detail", "Data len:" + dataLen);
//                                        // printData(frameData.toByteArray());
//                                        int[] frameValues = combineRawDatas(frameData.toByteArray());
//                                        Log.d("UVC_Iso_Stream_Activity", "Print frame raw data combines");
//                                        Formatter formatter1 = new Formatter();
//                                        for (int i = 0; i < frameValues.length; i++) {
//                                            formatter1.format("%d ", frameValues[i]);
//                                            if ((i + 1) % 10 == 0) {
//                                                formatter1.format("\n");
//                                            }
//                                        }
//                                        // printBlobStr(formatter1.toString());
//                                        double[] frameTemperatures = new double[frameValues.length];
//                                        formatter1.flush();
//                                        Formatter formatter2 = new Formatter();
//                                        Log.d("UVC_Iso_Stream_Activity", "Print frame raw data to Temperatures");
//                                        double maxTemp = Double.MIN_VALUE, minTemp = MAX_VALUE;
//                                        for (int i = 0; i < frameTemperatures.length; i++) {
//                                            frameTemperatures[i] = raw2temperature(frameValues[i]);
//                                            formatter2.format("%7.3f ", frameTemperatures[i]);
//                                            if ((i + 1) % 10 == 0) {
//                                                formatter2.format("\n");
//                                            }
//                                            if (frameTemperatures[i] > maxTemp) {
//                                                maxTemp = frameTemperatures[i];
//                                            }
//                                            if (frameTemperatures[i] < minTemp) {
//                                                minTemp = frameTemperatures[i];
//                                            }
//                                        }
//                                        maxTemperature = maxTemp;
//                                        minTemperature = minTemp;
//                                        Log.d("frameTemperatures", String.format("Max: %.6f; Min: %.6f", maxTemperature, minTemperature));
//                                        //printBlobStr(formatter2.toString());
//                                    }

                                    /* Codec to video format */
                                    if (videoformat.equals("MJPEG")) {
                                        try {
                                            //log("Frame, len= " + frameData.size());
                                            processReceivedMJpegVideoFrameKamera(frameData.toByteArray());
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    } else if (videoformat.equals("YUV")) {
                                        processReceivedVideoFrameYuv(frameData.toByteArray(), Videoformat.YUV);
                                    } else if (videoformat.equals("YUY2")) {
                                        processReceivedVideoFrameYuv(frameData.toByteArray(), Videoformat.YUY2);
                                    } else if (videoformat.equals("YV12")) {
                                        processReceivedVideoFrameYuv(frameData.toByteArray(), Videoformat.YV12);
                                    } else if (videoformat.equals("YUV_420_888")) {
                                        processReceivedVideoFrameYuv(frameData.toByteArray(), Videoformat.YUV_420_888);
                                    } else if (videoformat.equals("YUV_422_888")) {
                                        processReceivedVideoFrameYuv(frameData.toByteArray(), Videoformat.YUV_422_888);
                                    } else if (videoformat.equals("UYVY")) {
                                        processReceivedVideoFrameYuv(frameData.toByteArray(), Videoformat.UYVY);
                                    }
                                    frameData.reset();
                                }
                            }
                        }
                        if (reapTheLastFrames) {
                            if (++lastReapedFrames == activeUrbs) break;
                        } else {
                            req.initialize();
                            req.submit();
                        }
                        if (stopKamera == true) {
                            reapTheLastFrames = true;
                            break;
                        }
                    }
                }
                log("OK");
            } catch (Exception e) {
                e.printStackTrace();
            }
            runningStream = null;
        }
    }

    private int[] combineRawDatas(byte[] rawDatas) {
        int[] combines = new int[(rawDatas.length / 2)];
        for (int i = 0, j = 0; i < (rawDatas.length / 2); i++, j += 2) {
            int front = rawDatas[j] & 0xff;
            int back = rawDatas[j + 1] & 0xff;
            combines[i] = (front << 8) | back;
        }
        return combines;
    }

    // -- define Flir calibration values ---------------
    private final static double PlanckR1 = 16528.178;
    private final static double PlanckB = 1427.5;
    private final static double PlanckF = 1;
    private final static double PlanckO = -1307.0;
    private final static double PlanckR2 = 0.012258549;
    private final static double TempReflected = 20.0;     // Reflected Apparent Temperature [°C]
    private final static double Emissivity = 0.95;

    private double raw2temperature(int raw) {
        // mystery correction factor
        raw *= 4;
        // calc amount of radiance of reflected objects ( Emissivity < 1 )
        double RAWrefl = PlanckR1 / (PlanckR2 * (Math.exp(PlanckB / (TempReflected + 273.15)) - PlanckF)) - PlanckO;
        // get displayed object temp max/min
        double RAWobj = (raw - (1 - Emissivity) * RAWrefl) / Emissivity;
        // calc object temperature
        return PlanckB / Math.log(PlanckR1 / (PlanckR2 * (RAWobj + PlanckO)) + PlanckF) - 273.15;
    }

    public class IsochronousStreamLibUsb extends Thread {
        private Activity activity;
        private boolean reapTheLastFrames;
        private int lastReapedFrames = 0;
        private boolean write = false;
        private int framecnt = 0;
        ExecutorService mExecutorThread = Executors.newSingleThreadExecutor();

        public IsochronousStreamLibUsb(Context mContext) {
            setPriority(Thread.MAX_PRIORITY);
            activity = (Activity) mContext;
        }

        public void run() {
            try {
                mExecutorThread.execute(new Runnable() {
                    public void run() {
                        JniIsoStreamActivity(1, 1);
                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }
            log("OK");
            runningStreamLibUsb = null;
        }
    }

    private boolean checkFrameSize(int size) {
        if (size < 6000) return true;
        if (differentFrameSizes == null) differentFrameSizes = new int[5];
        if (lastThreeFrames == null) lastThreeFrames = new int[4];
        lastThreeFrames[3] = lastThreeFrames[2];
        lastThreeFrames[2] = lastThreeFrames[1];
        lastThreeFrames[1] = lastThreeFrames[0];
        lastThreeFrames[0] = size;
        if (++whichFrame == 5) whichFrame = 0;
        if (differentFrameSizes[whichFrame] == 0) {
            differentFrameSizes[whichFrame] = size;
            return false;
        }
        if (differentFrameSizes[whichFrame] < size) {
            differentFrameSizes[whichFrame] = size;
            return false;
        }
        int averageSize = 0;
        for (int j = 1; j < lastThreeFrames.length; j++) {
            averageSize = averageSize + lastThreeFrames[j];
        }
        if ((averageSize / 3 - 1000) < size) {
            return false;
        }
        if ((averageSize / 3 / 1.3) < size) {
            log("averageSize = " + (averageSize / 3 / 1.3));
            return false;
        }
        if (size == imageWidth * imageHeight * 2) return false;
        return true;
    }

    private void fetchTheValues() {
        camStreamingAltSetting = 2;
        videoformat = "UYVY";
        camFormatIndex = 1;
        imageWidth = 80;
        imageHeight = 60;
        camFrameIndex = 1;
        camFrameInterval = 1111111;
        packetsPerRequest = 8;
        maxPacketSize = 962;
        activeUrbs = 1;
        bUnitID = 0;
        bTerminalID = 0;
        bNumControlTerminal = null;
        bNumControlUnit = null;
        bcdUVC = null;
        bStillCaptureMethod = 0;
        LIBUSB = false;
        moveToNative = false;
    }

    // Debug 用
    private static void printData(byte[] formatData) {
        Formatter formatter = new Formatter();
        for (int i = 0; i < formatData.length; i++) {
            formatter.format("0x%02x ", formatData[i]);
        }
        String hex = formatter.toString();
        printBlobStr(hex);
        Log.d("printData", "hex count: " + formatData.length);
    }

    public static void printBlobStr(String str) {
        str = str.trim();
        int index = 0;
        int maxLength = 0x020000;
        String sub;
        while (index < str.length()) {
            if (str.length() <= index + maxLength) {
                sub = str.substring(index);
            } else {
                sub = str.substring(index, maxLength);
            }

            index += maxLength;
            Log.i("printBlobStr", sub.trim());
        }
    }

    private final String getUSBFSName(final UsbDevice ctrlBlock) {
        String result = null;
        final String name = ctrlBlock.getDeviceName();
        final String[] v = !TextUtils.isEmpty(name) ? name.split("/") : null;
        if ((v != null) && (v.length > 2)) {
            final StringBuilder sb = new StringBuilder(v[0]);
            for (int i = 1; i < v.length - 2; i++)
                sb.append("/").append(v[i]);
            result = sb.toString();
        }
        if (TextUtils.isEmpty(result)) {
            log("failed to get USBFS path, try to use default path:" + name);
            result = DEFAULT_USBFS;
        }
        return result;
    }


    public int getBus(String myString) {
        if (myString.length() > 3)
            return parseInt(myString.substring(myString.length() - 7, myString.length() - 4));
        else
            return 0;
    }

    public int getDevice(String myString) {
        if (myString.length() > 3)
            return parseInt(myString.substring(myString.length() - 3));
        else
            return 0;
    }

    private int videoFormatToInt() {
        if (videoformat.equals("MJPEG")) return 1;
        else if (videoformat.equals("YUY2")) return 0;
        else return 0;
    }

    class Frame {
        public Pointer frameData;
        public int len;
    }

    private final SurfaceHolder.Callback mSurfaceViewCallback = new SurfaceHolder.Callback() {
        @Override
        public void surfaceCreated(final SurfaceHolder holder) {
            log("surfaceCreated:");
            Canvas canvas = mUVCCameraView.getHolder().lockCanvas();
            canvas.drawColor(Color.GRAY, PorterDuff.Mode.SRC);
            mUVCCameraView.getHolder().unlockCanvasAndPost(canvas);

        }

        @Override
        public void surfaceChanged(final SurfaceHolder holder, final int format, final int width, final int height) {
            if ((width == 0) || (height == 0)) return;
            log("surfaceChanged:");
            mPreviewSurface = holder.getSurface();
        }

        @Override
        public void surfaceDestroyed(final SurfaceHolder holder) {
            log("surfaceDestroyed:");
            mPreviewSurface = null;
        }
    };
}
